package deco2800.thomas.entities;

public interface HasHealth {
    int getHealth();
    void setHealth(int health);
}